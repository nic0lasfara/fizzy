# Changes

## Version 2.4.0

### User changes

* Remove dependency `thor`: now fizzy is without any dependency!
  You just need the ruby interpreter :D
  [@alem0lars][@alem0lars]

### Dev changes



## Version 2.3.4

### User changes

* Fix filters apply
  [@alem0lars][@alem0lars]

## Version 2.3.3

### User changes

* Minor fixes
  [@alem0lars][@alem0lars]

## Version 2.3.2

### User changes

* Add basic questions completion (if readline is available)
  [@alem0lars][@alem0lars]
* Minor bug fixes
  [@alem0lars][@alem0lars]

### Dev changes

* Refactor meta commands into separate classes.
  [@alem0lars][@alem0lars]
* Refactor `colorize` (for strings colorization), using an internal parser
  instead of using gem thor.
  All output strings have been updated to use the new format string.
  [@alem0lars][@alem0lars]
* Refactor `quiz` into `ask`, using an internal implementation instead of using
  gem thor.
  [@alem0lars][@alem0lars]

## Previous versions

See the release notes.


<!-- Link declarations -->

[@alem0lars]: https://github.com/alem0lars
[@jak3]:      https://github.com/jak3
